from Crypto.Cipher import AES
import base64
import os

BLOCK_SIZE = 16
PADDING = '{'
#padding the block size
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING

EncodeAES = lambda c, s: base64.b64encode(c.encrypt(pad(s)))
DecodeAES = lambda c, e: c.decrypt(base64.b64decode(e)).rstrip(PADDING)

#aes key
secret = os.urandom(BLOCK_SIZE)


cipher = AES.new(secret)

key='ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDGOXzqxD5TwvjTEn82xx7DWgzgACrBBxa3HoK5q0XV8Y/NP/Nl+4ZYAUYCJLBsw+8dun3ogghRuX6rBpeIotSfxT4PpH+EVogtlE3bhzb6zg9upgBnJai5JqqQeVLoTwBHDcQrvFYYZWGpxApQ0e75OARFiAVhKh9beeP7OD+Bo8dSux216+UvxEoaWICy4eO15iGd6YUhVbobu9y0DmWvIcoMh/xgRaR1kP4pQK3eC6/y8DuwlPwjFcRPG5pDdRUFeg5eamWMnQoa5it36WlJlfCuN/tUsUuW/pVlzWIt+oO1NsK3nxFNb61NtVm3y/RCA1lKAyKoiswzwiGmXVOL ayushsaxena@Ayush-Saxena.local'

#encrytped string
encoded = EncodeAES(cipher,key)
print 'Encryption:',encoded

#actual string
decoded = DecodeAES(cipher, encoded)
print  'Decryption:',decoded