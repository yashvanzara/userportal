from flask import Flask, render_template, request
import pyrebase
import encrypt


app = Flask(__name__)
config = {
  "apiKey": "AIzaSyDDsSxdM5rAUgSimd5EbSYAoyuaH3qdWV",
  "authDomain": "awesomeproj123.firebaseapp.com",
  "databaseURL": "https://awesomeproj123.firebaseio.com/",
  "storageBucket": "awesomeproj123.appspot.com",
  "serviceAccount": "/Users/alizeyjalil/Downloads/awesomeproj123-firebase-adminsdk-hlndr-dd7b97537c.json"
}

firebase = pyrebase.initialize_app(config)
db = firebase.database()
#key and initialisation vector for encrypting usernamev ENCRYPTING IS PRODUCING ERRORS IN PYTHON
#key = b'This is a key123'
#encryption_suite = AES.new(key, AES.MODE_CBC)

@app.route('/', methods=['GET', 'POST'])
def homepage():
    if request.method == 'GET':
        return render_template('homepage.html')

    elif request.method == "POST":
        curr_username = str(request.form['username'])
        #print(curr_username)
        #cipher_username = encryption_suite.encrypt(curr_username)

        if db.child("users").child(curr_username).get().val() == None:
            servername = ""
            sudop = "no"
            if request.form.get("development"):
                servername = servername + request.form['development'] + ","
            if request.form.get("production"):
                servername = servername + request.form['production'] + ","
            if request.form.get("qa"):
                servername = servername + request.form['qa']

            if request.form.get("sudoprev"):
                sudop = "yes"

            #encrypted_pub_key = encrypt.encryption(request.form['pubkey'])
            #encrypted_password = encrypt.encryption(request.form['password'])
            #print(encrypted_pub_key)
            #print(encrypted_password)

            data = {"name": request.form['firstname'] + " " + request.form['lastname'],
                    "user_name": curr_username,
                    "password": request.form['password'],
                    "pub_key": request.form['pubkey'],
                    "server_name": servername,
                    "sudo_prev": sudop}

            db.child("users").child(curr_username).set(data)
            return "Added to database"
        else:
            return 'Already exists'


@app.route('/delete', methods=['GET', 'POST'])
def del_page():
    if request.method == 'GET':
        return render_template('del_page.html')
    elif request.method == 'POST':
        todel = db.child("users").child(request.form['username']).get().val()
        db.child("todelete").child(request.form['username']).set(todel)
        db.child("users").child(request.form['username']).remove()
        return 'Deleted from the database if it actually did exist!'

@app.route('/update', methods=['GET', 'POST'])
def update_page():
    if request.method == 'GET':
        return render_template('update_page.html')
    elif request.method == 'POST':

        if db.child("users").child(request.form['username']).get().val() == None:
            return "User doesn't exists"

        pw = request.form['password']
        #print("password "+ pw)
        gpw = db.child("users").child(request.form['username']).get().val()['password']
        #print("fetched pass " + gpw)
        #en_pw = encrypt.encrytion(pw)
        #print(en_pw)

        if (pw == gpw):
            #encrypted_pub_key = encrypt.encryto(request.form['pubkey'])
            db.child("users").child(request.form['username']).update({"pub_key": request.form['pubkey']})

        else:
            return "Wrong password"
        #db.child("users").child(request.form['username']).update({"pub_key": request.form['pubkey']})
        return 'Updated for the username'



#the script which runs the flask app, allows us to run the application
if __name__ == '__main__':
    app.run(debug=True, use_reloader=True)