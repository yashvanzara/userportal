
class Pound:
    # defining a constructor now for classes, used with __init__, if you want to just define different funcs, we'll
    # methods for the class
    def __init__(self, first_name, last_name, user_name, public_key):
        # in place of this we use self
        self.first_name = first_name
        self.last_name = last_name
        self.user_name = user_name
        self.public_key = public_key


